package com.kovansys.mvp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class OrderItem {
	
	@Id
	private long orderitemid;
	
	@ManyToOne
	@JoinColumn(name = "orderid")
	private CustomerOrder order;
	
	@OneToOne
	@MapsId
	@JoinColumn(name = "skuid")
	private Sku sku;
	private int quantity;
	private String status;
	
	protected OrderItem(){}
	
	@Override
    public String toString() {
        return String.format(
                "Order[%d, %d, %s]",
                orderitemid, quantity, status);
    }
}
