package com.kovansys.mvp;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Sku {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long skuid;
	
	private String upccode;
	private String description;
	private int length;
	private int width;
	private int height;
	private int weight;
	private boolean keepvertical;
	private int nesteddimension;
	private int nestingincrement;
	
	@Temporal(value=TemporalType.DATE)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Date expirationdate;
	
	protected Sku() {}

    public Sku(String upccode, String description, int length, int width, int height, int weight, boolean keepvertical, int nesteddimension, int nestingincrement) {
        this.upccode = upccode;
        this.description = description;
        this.length = length;
        this.weight = width;
        this.height = height;
        this.weight = weight;
    	this.keepvertical = keepvertical;
    	this.nesteddimension = nesteddimension;
    	this.nestingincrement = nestingincrement;
    }
	
    
    @Override
    public String toString() {
        return String.format(
                "Sku[SKU Id=%d, UPC Code='%s', Description='%s']",
                skuid, upccode, description);
    }

	
		
}
