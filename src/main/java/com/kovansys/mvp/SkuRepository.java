package com.kovansys.mvp;

import org.springframework.data.repository.CrudRepository;

public interface SkuRepository extends CrudRepository<Sku, Long> {

}
