package com.kovansys.mvp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Rack {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long rackid;
	private long rackdivisionrecipe;
	
	protected Rack(){}
	
	@Override
    public String toString() {
        return String.format(
                "Rack[Rack Id=%d, Rack Division Recipe='%d']",
                rackid, rackdivisionrecipe);
    }
}
