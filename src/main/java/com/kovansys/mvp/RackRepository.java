package com.kovansys.mvp;

import org.springframework.data.repository.CrudRepository;

public interface RackRepository extends CrudRepository<Rack, Long> {

}
