package com.kovansys.mvp;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class CustomerOrder {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderid;
	private long customerid;
	private int priority;
	
	@Temporal(value=TemporalType.DATE)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Date createdon;
	
	@Temporal(value=TemporalType.DATE)
	private Date processedon;
	
	private String status;
	
	protected CustomerOrder(){}
	
	@Override
    public String toString() {
        return String.format(
                "Order[%d, %d, %d, %s, %s, %s]",
                orderid, customerid, priority, createdon, processedon, status);
    }
}
